package compteurswing;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class fenetrecompteur extends JFrame
{
	int min=-11;
	int valeur=0;
	int max=11;
	JLabel mini=new JLabel("-11");
	JLabel compt=new JLabel("0",JLabel.CENTER);
	JLabel maxi=new JLabel("11");
	JTextField bouh=new JTextField(7);
	public fenetrecompteur()
	{
		super("compteur") ;
		ActionListener incr=new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(valeur<max){
					valeur++;
					compt.setText(""+valeur);
					compt.revalidate();
				}
			}
		};
		ActionListener decr=new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(valeur>min){
					valeur--;
					compt.setText(""+valeur);
					compt.revalidate();
				}
			}
		};
		setPreferredSize(new Dimension(222,125)) ;
		setLayout(new BorderLayout()) ;
		JPanel toto=new JPanel();
		JButton decrb=new JButton("-1");
		decrb.addActionListener(decr);
		toto.add(decrb);
		JButton incrb=new JButton("+1");
		incrb.addActionListener(incr);
		toto.add(incrb);
		add(mini,BorderLayout.WEST) ;
		add(compt,BorderLayout.CENTER) ;
		add(maxi,BorderLayout.EAST) ;
		add(toto,BorderLayout.SOUTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//definition de la barre de menu
		JMenuBar barreMenus = new JMenuBar() ;
		JMenu menu1 = new JMenu("Fichier") ;
		JMenuItem jmi1 = new JMenuItem("Quitter");
		jmi1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);	
			}
		});
		menu1.add(jmi1) ;
		barreMenus.add(menu1);
		JMenu menu2 = new JMenu("Modifier") ;
  		JMenuItem jmi2 = new JMenuItem("-1") ;
  		jmi2.addActionListener(decr);
		menu2.add(jmi2) ;
		JMenuItem jmi3 = new JMenuItem("+1") ;
		jmi3.addActionListener(incr);
		menu2.add(jmi3) ;
		barreMenus.add(menu2);
		JMenu menu3 = new JMenu("Intervalle") ;
		JMenuItem jmi4 = new JMenuItem("D�finir valeur minimale") ;
		jmi4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			ouvref(-1);
			}
		});
		menu3.add(jmi4) ;
		JMenuItem jmi5 = new JMenuItem("D�finir valeur maximale") ;
		jmi5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			ouvref(1);
			}
		});
		menu3.add(jmi5) ;
		barreMenus.add(menu3);
		this.setJMenuBar(barreMenus) ;
		
		pack();
		setResizable(false);
		setVisible(true) ;
	}
	public static void main(String[] a)
	{
		new fenetrecompteur() ;
	}
	public void ouvref(final int borne){
		final JDialog didi=new JDialog(this,"Modification Borne",true);
		didi.setLayout(new BorderLayout());
		didi.add(new JLabel("Entrez la nouvelle valeur :",JLabel.CENTER),BorderLayout.NORTH);
		didi.add(bouh,BorderLayout.CENTER);
		JButton ok=new JButton("OK");
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				if(borne==1){
					max=Integer.parseInt(bouh.getText());
					maxi.setText(""+max);
					maxi.revalidate();
					if(valeur>max){
						valeur=max;
						compt.setText(""+valeur);
						compt.revalidate();
					}
				}else{
					min=Integer.parseInt(bouh.getText());
					mini.setText(""+min);
					mini.revalidate();
					if(valeur<min){
						valeur=min;
						compt.setText(""+valeur);
						compt.revalidate();
					}
				}
				bouh.setText("");
				didi.setVisible(false);
			}
		});
		didi.add(ok,BorderLayout.SOUTH);
		didi.pack();
		didi.setResizable(false);
		didi.setVisible(true) ;
	}
}
